﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Northwin.Business.Abstruct;
using Northwind.DataAccess.Abstruct;
using Northwind.Entities.Concrete;

namespace Northwin.Business.Concrete
{
    public class ProductManager : IProductService
    {
        private IProductDal _productDal;

        public ProductManager(IProductDal productdal)
        {
            _productDal = productdal;
        }

        public List<Product> GetAll()
        {
            return _productDal.GetAll();
        }

        public List<Product> GetPrdouctsByCategory(int categoryId)
        {
            return _productDal.GetAll(p => p.CategoryId == categoryId).ToList();
        }

        public List<Product> GetProductByProductName(string productName)
        {
            return _productDal.GetAll(p => p.ProductName.ToLower().Contains(productName.ToLower())).ToList();
        }

        public void Add(Product product)
        {
            _productDal.Add(product);
        }
    }
}
