﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Northwind.Entities.Concrete;

namespace Northwin.Business.Abstruct
{
  public  interface IProductService
  {
      List<Product> GetAll();
      List<Product> GetPrdouctsByCategory(int categoryId);
      List<Product> GetProductByProductName(string productName);
      void Add(Product product);
  }
}
